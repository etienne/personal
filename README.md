# Etienne's personal website
This is the source code of my personal website: [https://etiennebaque.com](https://etiennebaque.com). Feel free to clone this repo as a starting point to create your own website.

### Prerequisites
This website was made using Jekyll. You'll have to install it first locally, along with Ruby. More info [here](https://jekyllrb.com/docs/installation/).

