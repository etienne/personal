---
title: Linux software for music orchestration
permalink: /music/2021/03/15/linux-music-orchestration/
layout: page
category: music
---

## Linux software for music orchestration

_Published on March 15th, 2021_

At the beginning of my [music orchestration course](https://www.udemy.com/course/orchestrationcourse), I was introduced to some piece of software to use as part of this activity. The instructor did a great job at listing a range of software, from the cheap/free one offering basic features to the most expensive ones, mainly for professional uses. She also listed additional libraries to get on top of chosen software.

That said, most of the presented software were only available on Windows or MacOS. So as a long-time Linux (Debian) user currently learning music orchestration, I decided to find out what the notable Free/Libre packages were, and which one I would use for the rest of the course as well as for the foreseeable future.

### Music notation: MuseScore

The music notation software that I started to use as part of the course is [MuseScore 2](https://musescore.org/en/download) (I'll simply call it MuseScore). This seems to be the most popular fully-featured music notation software available on Debian, as well as other Linux distros, Windows and MacOS.

I don't have much to say at the moment, I just learned about how to do a piano reduction of a String quartet piece and MuseScore does have that reduction functionality, which makes this operation easy to perform. So far so good.

MuseScore does not look like it's as fully featured as the software used by the teacher, [Sibelius](https://www.avid.com/sibelius), which seems to be the big reference music notation software in this industry (but, as you may have guessed, it's only available on MacOS and Windows). But MuseScore will do for me.

### DAW: Ardour

Later on, I may need to use a DAW software too. While a music notation software aim is to write/compose music on a sheet music, the DAW (digital audio workstation) is more designed to manipulate audio directly. A notation software can't manipulate audio.

I have not learned to use a DAW yet, I'll do that towards the end of the course (this is mainly a course about orchestration and composition - so a notation software is more relevant here). However, I looked into what DAWs existed on Linux out there. The popular one seems to be [Ardour](https://community.ardour.org/download). I can't say much about it for now.

{% include back-to-music.html %}
