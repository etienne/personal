---
title: BBC Symphonic Orchestra with Ardour
permalink: /music/2023/02/06/bbcso-with-ardour/
layout: page
category: music
---

## BBC Symphonic Orchestra with Ardour

_Published on February 6th, 2023_

Last time I looked at VST libraries was [a while
ago]({{site.url}}/music/2021/09/15/diving-into-ardour), when I ended up using
the [Virtual Playing Orchestra](http://virtualplaying.com/virtual-playing-orchestra/). This library did a fine job at what I wanted to achieve. Back then, I also mentioned some limitations: the fact that better sound samples are provided only for Windows and MacOS systems.

This post here is an update on this statement: after some investigation, I
managed to import and use the [BBC Symphonic Orchestra plugin](https://www.spitfireaudio.com/bbcso) (BBCSO) into Ardour (given that this library is initially downloadable for Windows and MacOS) on a Debian 11 system! Happy to understand how to push music production further on Linux systems further, as the BBCSO plugin provides great quality sound samples!

This article will explain how I did this. Hopefully, it will be helpful for you. At this point, I will assume that you're at least somewhat familiar with a Linux system, as it'll get (a bit) technical.

### Prerequisite: Ardour 7.2

At time of writing, I'm on Debian 11 stable. The Ardour version available for
this distro is 6.5. I didn't manage to install and use BBCSO with Ardour 6.5, as it is out of
date for what we want to achieve here.

Upgrading to the latest version of Ardour (7.2) did the trick. So make sure to
get at least this version if you want to give this BBCSO plugin a try.

You can download the latest version of Ardour [from this page](https://community.ardour.org/download).

### Sign up to Spitfire Audio and download their app

I've been using the [BBCSO "Discover" edition](https://www.spitfireaudio.com/bbc-symphony-orchestra-discover), which is the free version of the library. The SpitfireAudio website says the free version provides 33 instruments which is pretty good for a free library.

First, you will need to open an account on [spitfireaudio.com](https://www.spitfireaudio.com), which is where the BBCSO library is provided. Then you can download the Spitfire Audio app from [this page](https://www.spitfireaudio.com/info/library-manager/). No Linux version is provided, so download the Windows version of the app (yes, even if you're on Linux).

### Download Wine to run Spitfire Audio 

At this point, you will need to download and use [Wine](https://www.winehq.org/).

Wine is a fantastic program that allows to run Windows programs on Linux. This
is what we will be using to install and use the Spitfire Audio app.

Go to [this page](https://wiki.winehq.org/Download), choose your binary or
install from source. You will want to install the `Staging branch` version of
Wine, not the version from the `Stable branch`. The reason for that is that the
other piece of software which is talked about in the next section of this post specifically
mentions the use of Wine Staging.

The Wine documentation is pretty good and quite detailed, you should be able to install Wine Staging without any problem.

Once you've done so, check your Wine version:

```
> wine --version
wine-8.0 (Staging)
```

Then you should be good to install Spitfire Audio, using the Windows client file
you downloaded from the Spitfire Audio website:

```
wine SpitfireAudio-Win-3.4.4.exe
```

Once you've installed it, you'll be able to run the Spitfire Audio app. From
within the application, after login in using your Spitfire account you just created:

1. Click on "Discover" in the "My Products" section, then download "BBCSO Discover" and "BBCSO Plugin" to download the actual BBCSO plugin.
1. While you have the app open, you can also download some sound libraries from the LABS plugin, which is different than the BBCSO plugin. The sound libraries accessible via the LABS plugin are also quite interesting, make sure to check them out. To do so, click on the "LABS" menu and download the sound libraries that are available.

After you're satisfied with the samples you downloaded, you can quit the Spitfire Audio
application.

### Yabridge setup

Now that you've got your BBCSO (and LABS) plugins, we need to use
[yabridge](https://github.com/robbert-vdh/yabridge) to be able to use the files
related to these plugins (VST files) into Linux.

Follow [these instructions](https://github.com/robbert-vdh/yabridge#usage) to
install a recent version of yabridge and then use the `yabridgectl` utility to
make the VST files available on your machine. After adding the `yabridge` folder
to your `$PATH`, here are the commands I ended up running:

```
# Referencing VST files initially installed in Wine (through the Spitfire Audio app)
yabridgectl add ~/.wine/drive_c/Program\ Files/VstPlugins
yabridgectl add ~/.wine/drive_c/Program\ Files/Common\ Files/VST3

# Making them available in Linux
yabridgectl sync

# Check that syncing went well
yabridgectl status
```

If you can see that your VST files are made available in `~/.vst/yabridge` and
`~/.vst3/yabridge`, then you're done with this section.

### Import VST files in Ardour

Almost there! You just need to have Ardour scan these new `.vst` folders so that
it can pick up on the new VST files. To do so:

1. Open Ardour
1. Click on **Edit** > **Preferences**, then choose the "VST". under
   "Plugins".
1. In the "VST 2.x" section, click on "Edit", and add the `~/.vst/yabridge`
   path.
1. In the "VST 3" section, click on "Edit", and add the `~/.vst3/yabridge`
   path.
1. Select the "Plugins" section and click on `Scan for Plugins`. The BBCSO
   plugins should be scanned and found.
1. Back to the main screen, add a new MIDI track, then select one of the two
   options:
   - **LABS**: this will open the LABS plugin with the sound libraries you
     downloaded earlier from the LABS menu.
   - **BBC Symphonic Orchestra**: this will open the BBCSO plugin that provides
     libraries with Strings, Woodwinds, Brass and Percussions samples.

Once you've added that MIDI track, double-click on the added plugin in the
mixer strip to show its UI. Here's below a screenshot of the LABS plugin:

[![LABS in Ardour]({{ site.url }}/assets/img/posts/labs-ardour.png){:class="img-responsive"}]({{ site.url }}/assets/img/posts/labs-ardour.png)

And here's a screenshot of the BBCSO plugin:

[![BBCSO in Ardour]({{ site.url }}/assets/img/posts/bbcso-ardour.png){:class="img-responsive"}]({{ site.url }}/assets/img/posts/bbcso-ardour.png)

### What's next?

I'm going to give the BBCSO plugin a try with the MIDI files I exported a while
ago from MuseScore when I was composing two little pieces of music, and see how
they render.

The next big step after that will be to get a MIDI keyboard and really start
utilizing what the BBCSO and LABS plugins have to offer. I'm leaning towards
getting the [AKAI MPK mini mk3](https://www.akaipro.com/mpk-mini-mk3). Based on
some reviews I read, this seems to be a good MIDI keyboard that's not too
expensive and that came out recently. Also, Ardour 7.2 seems to be [supporting this
brand and model out-of-the-box](https://github.com/Ardour/ardour/blob/master/share/midi_maps/AKAI_MPKmini.map) so yeah, let's give this a try!

{% include back-to-music.html %}
