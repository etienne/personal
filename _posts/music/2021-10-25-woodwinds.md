---
title: Woodwind instruments and orchestration
permalink: /music/2021/10/25/woodwinds/
layout: page
category: music
---

## Woodwind instruments and orchestration

_Published on October 25th, 2021_

After having completed the orchestration course for String instruments a few months ago, I started the second course of this great Orchestration series: a course about [orchestration for Woodwinds](https://www.udemy.com/course/orchestrationcourse2/)!

This course builds on top of the previous one: it goes over each family of Woodwind instruments: flutes, oboes, clarinets, bassoons and saxophones. I'm still in the middle of it, I went through the modules related to the four first families I just mentioned. There's quite a lot to wrap my head around: instrument ranges, specificities, orchestration considerations. Here's the list of families I reviewed so far, as well as a couple of comments for each item:

- Flute family: four instruments in this family: Flute, Piccolo, Alto Flute and Bass Flute. Flutes are generally weak in their low registers, but increasingly brilliant in their high registers.
- Oboe family: three group of instruments in this family: Oboes, English horns, other Oboes (which are not used as often). At the opposite to the flute family's dynamic range: strong in their low registers, become very weak in high registers.
- Clarinet family: three groups in this family: Clarinet (Bb, A), High Clarinets (Eb, D), Bass Clarinets, other Clarinets. Clarinets have the largest dynamic range of all woodwind instruments, and blends well with other instruments.
- Bassoon family: we have bassoons and contrabassoons in this family. Bassoons offer unique solo voice, both in low and high registers. Great for doubling.

This list is a very high-level presentation of the Woodwind instruments. But as it may suggests, there are so many technical and orchestration considerations to know about for each family (eg. depending of the chosen instrument for a specific family, a Woodwind instrument can project greatly or be drowned out easily, it can offer great doubling or should be used for solo, etc.), just as there were many techniques to go through during the String orchestration course. So the challenge with this new course here is different.

Importance of orchestration considerations also stand for the String instruments, but I feel like the wide range of Woodwinds instruments requires to pay more attention to these considerations if we want to justify the use of each type of instrument.

This requirement is compounded by the fact that many Woodwinds instruments don't sound as written: a Bb Clarinet means that when playing a C (written), the instrument will sound one tone lower, like a Bb. While a tone difference may be manageable from a composition point of view, it looks like other instruments are be trickier to deal with. For example, an English horn (which I think has a beautiful sound for melodies) sounds a perfect fifth lower than written. I don't know how this is going to be manageable from a composition point of view: maybe the music notation software may help with this type of specificity by applying that interval automatically? I don't know yet.

Anyway, these are more reasons to add a sort of Woodwind cheat sheet to my notes. But before doing so, I still need to go through the Saxophone family. Then I'll have to decide how many different instruments for each Woodwind family I want to pick for a future composition. Unlike the Strings, there's so many more options here! Let's see how it goes.

{% include back-to-music.html %}
