---
title: Know your (music notation) editor
permalink: /music/2021/04/21/know-your-editor/
layout: page
category: music
---

## Know your (music notation) editor

_Published on April 21st, 2021_

As part of the Music orchestration course for stringed instruments that I've been taking, I've been starting to use [MuseScore](https://musescore.org) as my music notation software.

When going through the final project of the course, the teacher asks students to spend 15-30 mins developing ideas for a theme / melody and write it on a music notation software. Even though she's been using Sibelius instead of MuseScore, I noticed that she's been pretty good at using keyboard shortcuts. And I like seeing her using that approach very much.

As a software developer, I fundamentally believe that it's very important to know how to navigate the tools used on a daily basis. I've been using a text editor called Vim for programming (and text editing) and I've been generally enjoying my overall workflow because I learned how to leverage what the tool has to offer (via key bindings) while there's still so much I don't know about it.

Before moving forward with using MuseScore (and later on, when using a DAW), I know by experience that I'll need to go through the learning curve of navigating this tool well (to some extent). That will take some practice, and this is something I already spent more time than the project exercise itself.

Whether it's in music composing, software development or in any activities requiring the use of an editor/software, we should always aim to be familiar with our tool, to know how to navigate it in order to improve our workflow effectively: the better our workflow is, the more we can spend time and fully focus on the actual core of our activities.

{% include back-to-music.html %}
