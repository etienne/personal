---
title: Project complete! Music and self-analysis
permalink: /music/2021/06/24/project-complete-analysis/
layout: page
category: music
---

## Project complete! Music and self-analysis

_Published on June 24th, 2021_

I finally managed to complete the orchestration project which is part of the [orchestration course for stringed instruments](https://www.udemy.com/course/orchestrationcourse) which I started almost a year ago.

This was a great experience and I'm rather satisfied with the final results! I managed to compose a short piece of a string orchestra (ensemble that includes a 1st violin, a 2nd violin, a viola, a cello and a double-bass).

- [String orchestra piece]({{ site.url }}/assets/music/orchestration_project.mp3) (MP3 file - produced with a [DAW](https://en.wikipedia.org/wiki/Digital_audio_workstation))
- [Sheet music]({{ site.url }}/assets/music/orchestration_project.pdf) (PDF file)

If you're giving this a listen/read, you'll see that this piece consists of an intro, a theme played by the cello, and then the theme played a second time by the 1st violin.

This was a great exercise and experience at the same time. Logically, it was first an opportunity for me to apply some techniques and bits of knowledge that I had learned during the course. But as the exercise went on and after I was done with this project, I noticed that some improvements could be made. Said differently, some details/gotchas became a bit clearer to me. And this is what this article is about: to list and write about these take-aways to make them more tangible for me.

### To keep a balanced orchestration is hard.

From the beginning I wanted to put a highlight on the cello. Even though it subscribes to the "low register" instruments, providing rich low notes, I wanted the cello to shine. That's why I made it play a theme mostly on the A string. That string plays high enough for a tune to be clear and expressive.

In order to give breathing room for the cello, I minimized the presence of the viola and violins. I think this is something I did quite ok when playing the theme the first time around.

When the theme played again (this time by the 1st violin) I still wanted to have the cello, with the help of the viola, to give some sort of counter-melody (eg. bars 18 and 20), with the aim to answer the theme played by the violin. But I felt like it didn't work, I felt like the violins were both "overpowering" the cello and viola. And this is this sort of balance that I want to talk about here: how to successfully play different parts that and complementary to one another, where exposing one instrument is not done to the detriment of another one.

Something that may help next time around is to remember each stringed instrument's "weakest string", the one string that has the weakest sound (and therefore gives naturally more room to other instruments): based on the course, for the violin, it's D string. For the viola, it's the G string. For the cello, it's also the G string. For the double-bass, it would be the A string. Keeping this in mind could help to craft balance better next time.

### "What should I do here with this instrument?"

This was a recurring question that I had the viola on a few occasions: the double-bass would support the chord being played across all instruments, the cello was "trying" to do a counter-melody, the 1st violin was playing the theme, and the 2nd violin was doubling the 1st violin. During the writing of the second occurrence of this theme, I was wondering what to do with the viola then.

Maybe this comes back to the topic of overall balance? Maybe this type of situation should be seen as an opportunity to rebalance? Maybe I should have used the viola more often to double the cello, bringing both instruments almost to the same register? I don't know.

Maybe these questions are a sign that I should have left more breathing room and simplify a few lines? I'm not sure. I'll need to learn when it's ok not use an instrument (or to have it play light articulations).

### Articulations to use with moderation.

The course shows a ton of articulations, which is quite daunting when going through them all the first time around. But I tried to apply some of them on this piece, I think the pianist within me took over this job: I just used the ones commonly used in piano: spiccato, slurs, louré. I also added a couple of crescendo and that's about it.

I see articulations like jars of spices in a kitchen: you can have dozens of different spices being available to you, but you'll end up using just a few.

To be able to use more "complex" articulations, I think that I have to see the need for it, or rather I need to nurture a type of inspiration that justify their use. To do so, I think I just need to spot them whenever I listen to a piece of music.

For example, I was listening to a concerto for violin by Mendelssohn today. I noticed a nice set of trills at the end of a section that was creating overall momentum. That articulation was well used here I find, I took note of it.

Even though I understand that articulations can be used in an infinite number of contexts (you can use whatever spice on whatever food in theory), I feel like at this stage I need to be shown when an articulation is used effectively.

### More observations?

That's about it for now. I'm going to have to listen to this piece again and see what other take-aways I can get from it. If I do, I'll update this list here :)

{% include back-to-music.html %}
