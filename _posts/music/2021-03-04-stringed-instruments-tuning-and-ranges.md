---
title: Stringed instruments - tuning and ranges
permalink: /music/2021/03/04/strings-tuning-ranges/
layout: page
category: music
---

## Stringed instruments: tuning and ranges

_Published on March 4th, 2021_

This is an attempt to show the tuning and ranges of all stringed instruments.

Each section shows one instrument and the clef each of them use. For each instrument, the diagram shows what its 4 strings are and what range each string can cover (with highest reachable note between parenthesis).

<pre>
------------|----1----|----2----|----3----|----4----|----5----|----6----|----7----|

                                                        E                    (G)
Violin                                           A               (E)
(Treble clef)                                D            (A)
                                      G               (D)

-----------------------------------------------------------------------------------

                                                 A                  (A)
Viola                                        D            (A)
(Alto-C clef)                         G               (D)
                                  C            (G)

------------|----1----|----2----|----3----|----4----|----5----|----6----|----7----|

                                       A                       (C)
Cello                              D            (A)
(Bass clef)                 G               (D)
                        C            (G)

-----------------------------------------------------------------------------------

                                      G                  (G)
Double Bass                        D            (A)
(Bass clef)                  A               (E)
                          E            (B)

------------|----1----|----2----|----3----|----4----|----5----|----6----|----7----|
</pre>

Some notes:

- The double bass is the only transposing instrument here: it sounds one octave lower than actually written: E2, A2, D3 and G3 (see above) sounds like E1, A1, D2 and G2.
- The double bass can have a C extension cord (C2 written, sounds like C1).

{% include back-to-music.html %}
