---
title: Strings and Woodwinds project complete! Music and analysis
permalink: /music/2022/07/14/strings-woodwinds-project-complete/
layout: page
category: music
---

## Strings and Woodwinds project complete! Music and analysis

_Published on July 14th, 2022_

About one year after finishing my [first orchestration project]({{ site.url }}/music/2021/06/24/project-complete-analysis), I finally managed to complete my second project: a small piece for Strings and Woodwinds.

This took me quite some time as I had limited availability to dedicate for these musical activities over the past few months. But little by little,
I managed to come up with something. There it is!

- [Strings and Woodwinds orchestra piece]({{ side.url
  }}/assets/music/strings_woodwinds_project.mp3) (DAW-produced MP3 file)
- [Sheet music]({{ site.url }}/assets/music/strings_woodwinds_project.pdf) (PDF
  file)

This is a piece of music written for the following instruments: Violins
1, Violins 2, Violas, Violoncellos, Double-basses, and also for Flutes, Oboes,
Clarinets in Bb and Bassoons. Like with the previous project, I composed a
piece of music that has an intro and a theme played twice (I revised [my previous
expectation to compose a Rondo]({{ site.url }}/music/2022/03/14/searching-balance-strings-woodwinds) pretty quickly, as it actually was a pretty ambitious goal haha).

I'm pretty happy with the end-result, I think this piece is a bit richer than
the previous one, more exploration was made possible because I had to deal with two instrument families this time,
and I had a longer theme here to play with. I also encountered some challenges, I asked myself a few
questions when orchestrating. I'll go through some take-away items.

### Theme design

I wanted to come up with a longer theme compared to what I had composed
previously, mostly because I wanted some room to create opportunities for
counter-melodies and other motifs.

I think I managed to achieve this by playing the same note over a couple of
measures at different times during the theme. For example, whenever the violins play a whole-note as part of the theme, there's room for the
Woodwinds to take over for a bit during that time. I think Woodwinds can really shine when they play a sort of
counter-melody, even when it is for a short amount of time. That creates an interesting
dynamic, an interesting mix.

The theme includes some triplets as well. Before introducing them, I was using
half-notes, justified by the fact that I wanted to create space for
experimentation, as mentioned above. But the use of half-notes here was detrimental to the
overall dynamic of the theme: the melody was feeling bare, a bit boring. So I thought mixing
some ternary over binary rhythm could fill this gap, and that's why I went for
some triplets here and there.

### Enjoying the Oboes / Violins combinations

Towards the end of the course on Woodwind orchestration, there's a small module
on instrument combinations. I leaned on this module to have an idea about which,
among the Woodwinds and the Strings, I could have together. I also
referred to the [Vienna Symphonic Library](https://www.vsl.co.at/en/Violin/Sound_Combinations) website which I had mentioned in a previous post.

I ended up linking how the timbre of the oboes sounds when playing a melody against
violins which present a counter-melody (or a background). This is a personal
preference, but this may be the point actually of all of this: one's vision of how a melody should be interpreted will be
reflected by one's feeling or tastes. It will also depend on what one has in
mind when wanting to describe an emotion or a scene. For example, should the melody of a theme from a
piece titled "Returning home" be played by a Clarinet or a Cello? It may depend
of the composer's vision of the piece.

I need to understand how to see a melody through the lens of each instrument,
and, for each of them, check if related feelings come out accordingly.

### Woodwinds for melodies

After experimenting with this music piece, I noticed that more Woodwind
instruments are generally more suited to play a melody (or a counter-melody) than Strings.
For example, if we consider low-register instruments only, I feel like bassoons are more suited to play a melody that
double-basses.

There was an interesting element that I had to pay attention to, when I wrote a
melody for Woodwinds (namely, the theme played by oboes): I had to make room for
breathing. This is something that I tried to do, I don't think it was too
detrimental to the theme itself.

I tried to use some Woodwinds as background when the theme was played
by the violins, the second time around. This was tricky, I tried to use the
bassoons to complement the double-bass, but I think I got this wrong. And
that's may be the biggest take-away for me here: the more instruments, the
trickier it gets to get the balance right.

My course instructor had warned me: the one thing beginner orchestrators will get
wrong is balance. I'm one of them, but through this project I got a better sense
of balance, so I'll try to do a better job next time!

{% include back-to-music.html %}
