---
title: Diving into Ardour
permalink: /music/2021/09/15/diving-into-ardour/
layout: page
category: music
---

## Diving into Ardour

_Published on September 15th, 2021_

A few months ago, I managed to compose my very first string orchestra piece, as part of a project. I wrote about it in [this post]({{ site.url }}/music/2021/06/24/project-complete-analysis).

At that point, I had only composed the sheet music (using MuseScore). But someone else offered to use a DAW (Digital Audio Workstation) to produce the music itself. I was very grateful about this, but I also knew that ramping up on learning how to use such software was going to be my next step.

### Ardour 6

There are many DAWs out there, but since I've been using Linux for more than a decade, it just made sense to me to look into DAWs which I would instal on my Debian distro. I quickly ended up on downloading [Ardour 6](https://ardour.org/) and decided to go with it. I'm almost certain that 99% of people opening Ardour for the first are just overwhelmed! There are so much jumping at you when opening this application!! I didn't know where to start at first.

After clicking around for a while and managing to import the MIDI file I had exported from MuseScore, I decided to reach out to folks on the Fediverse for some help, and Open Mastering delivered. Big time. I'm super grateful: we had a chat for close to two hours where he gave me many tips to get started on Ardour. If you're interested in music production, you may want [to connect with him](https://mastodon.social/@openmastering).

After our chat, I had enough information to move forward on my own. So thank you kind stranger for the jumpstart!

### The Ardour ecosystem I'm sticking with (for now)

Since I'm interested in composing pieces for a string orchestra, the first challenge for me was to find an orchestral sample library using the SFZ file format (usable by Ardour). Open Mastering recommended the on from [VSCO community](https://vis.versilstudios.com/vsco-community.html) but I ended up finding another which I think sounded (relatively) better: the library from [Virtual Playing Orchestra](http://virtualplaying.com/virtual-playing-orchestra/). It provides many samples for not only strings, but also brass, woodwinds and percussions.

After that, I needed a plugin (a sampler) to be able to import these samples onto Ardour and use them onto my MIDI tracks. I went for [liquisfz](https://github.com/swesterfeld/liquidsfz), which does the job well.

Finally, as it was recommended to me, I needed to add some reverb to add some realism to the overall piece, by recreating some room echo. I ended up choosing [Dragonfly Reverb](https://github.com/michaelwillis/dragonfly-reverb/) as it has quite a few presets, so I don't have to touch all these knobs to get some sort of results :)

And that's about it. I managed to decouple the cello part of my piece into two buses, one for "regular" (arco) playing, one for cello spiccato. I also tried to modify the velocity here and there, I felt like I was not applying the correct volume to each instrument. But hey! I managed to produce something! You can listen to it below. I'm sure there's a lot that could be better, but I'll figure it out over time.

* [String orchestra piece]({{ site.url }}/assets/music/orchestration_project_ardour.mp3) (MP3 file - produced with Ardour!)

### Limitations

There's something that I read a couple of times when looking into sample libraries: there will be limitations when using free libraries. And this is something that my orchestration teacher was saying too. These free libraries won't be as good as other big commercial counterpart, such as Kontakt. I understand the upfront investment necessary to get good quality samples.

That said, coming from the software development world, this is an interesting situation. Since I started using Linux, I always found that the tools serving me the best come from FOSS (Free and Open-Source Software). On a daily basis, for personal as well as professional usage, it would be very counterproductive to come back to proprietary software. And I'm putting the ethical conversation aside here, I'm just talking about workflow.

So in music production, knowing that the best raw materials out there is usable only through MacOS or Windows adds a different element into this conversation. I'm not thinking about switching OS, I'm sticking with Linux, Ardour and everything that comes with it. I just think that it was an interesting observation to make.

However, I should not blame the quality of the samples too much: maybe the biggest limitation here is myself, along with my lack of knowledge of Ardour haha :) Hopefully I'll learn to fine-tune my music production over time and produce better sounding orchestra pieces.

{% include back-to-music.html %}
