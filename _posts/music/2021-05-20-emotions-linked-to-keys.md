---
title: Emotions linked to musical keys
permalink: /music/2021/05/20/emotions-linked-to-keys/
layout: page
category: music
---

## Emotions linked to musical keys

_Published on May 20th, 2021_

I recently figured that different musical keys could convey different but specific enough feelings and emotions. So I attempted the following web search: "feelings associated with music keys" and I stumbled upon what I was looking for.

The following chart is from ["Emotions of the Musical" - Copyright Kevin Lessmann 2004](http://www.gradfree.com/kevin/some_theory_on_musical_keys.htm). It describes which musical keys are linked to which emotions:

- C Major: completely pure. Its character is: innocence, simplicity, naivety, children's talk.
- C minor: declaration of love and at the same time the lament of unhappy love. All languishing, longing, sighing of the love-sick soul lies in this key.

- Db Major:  a leering key, degenerating into grief and rapture. It cannot laugh, but it can smile; it cannot howl, but it can at least grimace its crying.--Consequently only unusual characters and feelings can be brought out in this key.
- D Major: the key of triumph, of Hallelujahs, of war-cries, of victory-rejoicing. Thus, the inviting symphonies, the marches, holiday songs and heaven-rejoicing choruses are set in this key.
- D minor: melancholy womanliness, the spleen and humors brood.
- D# minor: feelings of the anxiety of the soul's deepest distress, of brooding despair, of blackest depression, of the most gloomy condition of the soul. Every fear, every hesitation of the shuddering heart, breathes out of horrible D# minor. If ghosts could speak, their speech would approximate this key.

- Eb Major: the key of love, of devotion, of intimate conversation with God.
- E Major: noisy shouts of joy, laughing pleasure and not yet complete, full delight lies in E Major.

- F Major: complaisance and calm.
- F minor: deep depression, funereal lament, groans of misery and longing for the grave.
- F# Major: triumph over difficulty, free sigh of relief uttered when hurdles are surmounted; echo of a soul which has fiercely struggled and finally conquered lies in all uses of this key.
- F# minor: a gloomy key; it tugs at passion as a dog biting a dress. Resentment and discontent are its language.

- G Major: everything rustic, idyllic and lyrical, every calm and satisfied passion, every tender gratitude for true friendship and faithful love,--in a word every gentle and peaceful emotion of the heart is correctly expressed by this key.
- G minor: discontent, uneasiness, worry about a failed scheme; bad-tempered gnashing of teeth; in a word: resentment and dislike.

- Ab Major: key of the grave. Death, grave, putrefaction, judgment, eternity lie in its radius.
- Ab minor: grumbler, heart squeezed until it suffocates; wailing lament, difficult struggle; in a word, the color of this key is everything struggling with difficulty.
- A Major: this key includes declarations of innocent love, satisfaction with one's state of affairs; hope of seeing one's beloved again when parting; youthful cheerfulness and trust in God.
- A minor: tenderness of character.

- Bb Major: cheerful love, clear conscience, hope aspiration for a better world. A quaint creature, often dressed in the garment of night. It is somewhat surly and very seldom takes on a pleasant countenance. Mocking God and the world; discontented with itself and with everything; preparation for suicide sounds in this key.
- Bb minor: a quaint creature, often dressed in the garment of night. It is somewhat surly and very seldom takes on a pleasant countenance. Mocking God and the world; discontented with itself and with everything; preparation for suicide sounds in this key.
- B Major: strongly colored, announcing wild passions, composed from the most glaring colors. Anger, rage, jealousy, fury, despair and every burden of the heart lies in its sphere.
- B minor: this is as it were the key of patience, of calm awaiting one's fate and of submission to divine dispensation.

{% include back-to-music.html %}
