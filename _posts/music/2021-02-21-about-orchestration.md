---
title: Learning about music orchestration
permalink: /music/2021/02/21/about-orchestration/
layout: page
category: music
---

## Learning about music orchestration

_Published on February 2nd, 2021_

I have been learning music orchestration (for string instruments) for the past few months. I studied music when I was a kid and teenager, and orchestration is something that I wanted to know more about for years. I was quite lucky when I ended up on a [super, intense and great course on Udemy](https://www.udemy.com/course/orchestrationcourse) on that topic!

What's music orchestration? It is the process of bringing techniques from multiple music instruments together, in order to compose a piece of music for orchestra (the study of the music instrument techniques themselves is called instrumentation).

I've been enjoying this course so far, taking it a lesson at a time, whenever I can dedicate some time for it. I'm now deciding to take this learning further by logging and sharing my progress.

{% include back-to-music.html %}
