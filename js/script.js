var byId = function( id ) { return document.getElementById( id ); };

document.addEventListener("DOMContentLoaded", function () {
  var lightDarkImage = byId("lightDarkImage");
  var savedTheme = localStorage.getItem("theme");
  var isDark = savedTheme == null ? window.matchMedia('(prefers-color-scheme: dark)').matches : savedTheme == 'dark';

  if (isDark) {
    lightDarkImage.classList.add('light');
    lightDarkImage.classList.remove('dark');
    document.documentElement.className = 'dark';
  } else {
    lightDarkImage.classList.remove('light');
    lightDarkImage.classList.add('dark');
    document.documentElement.className = 'light';
  }

  byId("lightDarkLink").addEventListener("click", function(){
    var docElement = document.documentElement
    var currentTheme = docElement.className

    if (currentTheme == 'light') {
      docElement.className = 'dark';
      lightDarkImage.classList.add('light');
      lightDarkImage.classList.remove('dark');
      localStorage.setItem('theme', 'dark');
    } else {
      docElement.className = 'light';
      lightDarkImage.classList.remove('light');
      lightDarkImage.classList.add('dark');
      localStorage.setItem('theme', 'light');
    }
  });
});
