---
title: Nova-Scotia
permalink: /nouvelle-ecosse/
layout: page
---

## Guide de la Nouvelle-Ecosse

Ceci est un guide de tourisme pour les amis et la famille qui viennent nous
rendre visite en Nouvelle-Ecosse.

Comme nous habitons à Bridgewater, ce guide se concentre plus particulièrement
sur la région Rive-Sud de la province.

En plus de ce guide, et une fois sur place, si vous souhaitez avoir une bonne carte sur votre téléphone
pour vous aider à vous repérer, je recommande l'application [Organics
Maps](https://organicmaps.app/fr/). Avec cette application, vous n'aurez pas à
utiliser le réseau téléphonique Canadien pour faire fonctionner votre carte. Il
suffira juste de télécharger la carte de Nouvelle-Ecosse en avance sur votre
téléphone 👍

---

### Bridgewater

🏠 LCLC: Le super centre d'activités de Bridgewater !! Il y a:
- Une super bibliothèque, avec des livres en Francais pour petits et un petit espace de jeu. Il y a aussi des tables pour lire ou travailler.
- Une piscine: il faut une carte de membres ou bien payer pour y accéder.
  Je peux toujours passer ma carte de member si ça peux aider !
- Un café (Fancy Pants Express)
- Une patinoire: des sessions gratuites et publiques sont organisée de temps en
  temps. Ils prêtent même des patins.

💵 King Street : la rue commercante de Bridgewater, tout le long de la rivière. Des pubs, des petits
restaurant, une librairie, un endroit sympa pour les brunches (Jac's), etc.

☕ The Barn / Fancy Pants: deux cafés indépendants qui se trouvent pas loin de Treehouse. En plus de toutes sortes de cafés qu'ils ont, les deux endroits servent aussi quelques produits sans gluten.

🏠 [Musée DesBrisay](https://desbrisaymuseum.ca/): le petit musée de Bridgewater.

🌳 Promenade autour du Duck Pond: jolie petite promenade à faire au centre de
Bridgewater. Il y a un chemin connecte la mare au musée DesBrisay, donc les deux peuvent se faire en même
temps.

🌳 Promenade / Course à pied / Vélo sur le Centennial trail pour aller plus loin, dans
la nature. Le trail va très loin et traverse même plusieurs villes.

### Juste en dehors de Bridgewater

🏖️ Wile’s lake: A environ 5 mins en voiture au sud de Treehouse, c’est l’endroit le plus accessible depuis Treehouse si on veut se baigner ! Il y a une table de pique-nique à côté de l’eau. A la sortie du lac, il y a aussi un marché fermier qui vend divers produits (Wile’s Lake Farm Market)

🌳 Miller Point Peace Park: juste à la sortie de Bridgewater. C’est une très jolie forêt avec un petit sentier qui fait une boucle. Il y a des tables de picnic à 500 mètres du parking, près de l’eau. Super endroit pour un repas en plein air.

🏖️ Mushamush beach park: Un lac avec plage à 15 mins en voiture de Bridgewater. Très joli paysage et super pour les petits car le niveau d’eau n’est pas profond sur plusieurs mètres.

### Mahone Bay et aux alentours

🎈 Idée de sortie avec les enfants: il y a une aire de jeu sympa qui s’appelle “Tiny Tots Playground”. Vous pouvez vous garer en voiture derrière la pharmacie De Mahone Bay (“Pharmasave”) qui se trouve à côté.

☕ The Barn: Pas très loin, il y a le très beau café “The Barn”. Même enseigne qu'à Bridgewater mais plus grand. On peut y aller à pied depuis l’aire de jeu, ou bien depuis le centre de Mahone Bay. A voir !

🍺 Brasserie Saltbox: bières et cidres locaux. Musique Celtique le dimanche de 14h à 16h de Mars à Octobre.

🍽️ [Eli + Trix](https://www.eliandtrix.com/): bon restaurant qui sert beaucoup de plats sans gluten, et qui fait
des brunchs aussi. La rue principale de Mahone Bay, sur laquelle se trouve ce restaurant a des boutiques sympas à voir.

🍽️ [Chicory Blue](https://chicoryblue.ca/): moins de 5 minutes en dehors de Mahone Bay, en revenant sur Bridgewater. C'est épicerie "à l'ancienne", charmante, qui fait aussi restaurant. Ils servent des brunches. Et ils ont une superbe terrasse ouverte pendant l'été qui vaut le coup!

### Lunenburg

⛵ Le centre-ville et les quais sont vraiment à voir, avec le célèbre bateau Blue Nose II à voir aussi.

🏠 Knaut-Rhuland House Museum (musée), maison du XVIIIe siècle à visiter.

🏠 Bibliothèque municipale à voir: bâtiment historique (et la bibliothèque à aussi des bouquins en Français pour les enfants)

🎵 Concerts dans le parc Bandstand en ville, les Samedis de 14 à 15h pendant
l'été

### Du côté de Rosebay

☕ Rosebay General Store: un café / petit restaurant sympa qui fait aussi épicerie. Sympa de s’y arrêter après une balade dans le coin.

🌳 Indian Commons Path: jolie randonnée en forêt facile, plusieurs boucles différentes qu’on peut choisir de faire selon si on veut marcher plus ou moins. A quelques kilomètre du Rosebay general store, pratique pour un café après la marche.

🏖️🌳 Hirtle beach + Gaff Point trail: une jolie plage à 6 kms du general store, par laquelle on accède à la randonnée de Gaff Point, qui est une péninsule. La rando - partir de la plage vers la péninsule, en faire le tour, et revenir au point de départ: 10kms, environ 2h15 - 2h30.

### Du côté de LaHave

☕ 📖 [Boulangerie et librairie LaHave](https://www.lahavebakery.com/): très jolie boulangerie (qui fait
café aussi) qui se trouve à LaHave, à l'Est de Bridgewater. Beaucoup de produits
locaux vendu en arrière-boutique. Ca vaut le coup de prendre un café en terrasse
quand il fait beau. 

Il y a aussi une librairie que se trouve à l'arrière du batiment, à voir aussi.

🏠 [Fort Point museum](https://www.fortpointmuseum.com/): joli musée sur la communauté de LaHave, pas loin de la boulangerie.

### Du côté de Petite-Rivière

🏖️ Rissers Beach Provincial Park: plage qui fait boucle avec un chemin de promenade. Il y a une boutique qui vend des glaces pendant l’été (entre la plage et le chemin de promenade)

🍽️ Osprey’s nest: A 5 minutes au sud de la plage Rissers, juste à l’entrée de Petite-Rivière: restaurant de Petite-Rivière. Ils ont le meilleur “chowder” que j’ai pu goûter (soupe aux fruits de mer et pommes de terre). Et ils ont un menu spécialement pour les plats sans-gluten ! 

🏖️ Greenbay: très joli endroit un peu caché, avec de très belle maisons autour. Il y a une petite plage où il n’y a jamais de monde (mais il peut y avoir un peu de vent).
